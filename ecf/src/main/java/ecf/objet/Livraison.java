package ecf.objet;
import ecf.enu.Foin;
import java.util.List;
import java.util.ArrayList;

/**
 * Class qui représente les livraisons
 */
public class Livraison {

    private String numeroLot;
    private String dateLivraison;
    private Foin foinType;
    private Integer quantiterLot;
    private Entrepot entrepot;
    private static List<Livraison> livraisonRegistre = new ArrayList<>();

    public Livraison(String numeroLot, String dateLivraison, Foin foinType, Integer quantiterLot) {
        this.numeroLot = numeroLot;
        this.dateLivraison = dateLivraison;
        this.foinType = foinType;
        this.quantiterLot = quantiterLot;
    }

    /**
     * Methode qui permet a un salarié de récéptioner les livraisons
     * Chaque récéption est inscrite dans un registre
     * @param personne
     * @param livraison
     */
    // Méthode pour réceptionner la livraison par un salarié
    public static void receptionParSalarie(Personne personne, Livraison livraison) {
        Salarie.receptionnerLivraison(livraison, personne);
        livraisonRegistre.add(livraison);
    }

    /**
     * Méthode qui sert a afficher le registre des livraisons
     * @return
     */
    // Méthode pour afficher le registre des livraisons
    public static void afficherRegistre() {
        for (Livraison livraison : livraisonRegistre) {
            System.out.println("Numéro de lot : " + livraison.getNumeroLot());
            System.out.println("Date de livraison : " + livraison.getDateLivraison());
            System.out.println("Type de foin : " + livraison.getFoinType());
            System.out.println("Quantité de lot : " + livraison.getQuantiterLot());
            System.out.println("");
        }
    }

    //_____ GETTER _____//
    public String getNumeroLot() {return numeroLot;}
    public String getDateLivraison() {return dateLivraison;}
    public Foin getFoinType() {return foinType;}
    public Integer getQuantiterLot() {return quantiterLot;}
    public static List<Livraison> getLivraisonRegistre() {return livraisonRegistre;}

    //_____ SETTER _____//
}
