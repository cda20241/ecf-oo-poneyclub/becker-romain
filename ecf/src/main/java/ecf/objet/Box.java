package ecf.objet;
import java.util.List;
import java.util.ArrayList;
import java.text.DecimalFormat;


/**
 * Class qui représente les box
 */
public class Box {

    private String boxId;
    private Integer capaciter;
    private Integer quantiterFoinCourant;
    private Integer quantiterFoinMax;
    private List<Poney> poneyList;

    public Box(String boxId, Integer capaciter, Integer quantiterFoinCourant, Integer quantiterFoinMax) {
        this.boxId = boxId;
        this.capaciter = capaciter;
        this.quantiterFoinCourant = quantiterFoinCourant;
        this.quantiterFoinMax = quantiterFoinMax;
        this.poneyList = new ArrayList<>();
    }

    //_________________________ METHODE POUR LE CALCULE DU TAUX DE REMPLISSAGE EN POURCENTAGE ________________________//
    /**
     * Methode qui calcule te taux de remplisage en poucentage
     * @return
     */
    public double calculerTauxRemplissage() {
        return ((double) quantiterFoinCourant / quantiterFoinMax) * 100;
    }

    //_________________________ METHODE POUR AFFICHER LES TAUX DE REMPLISSAGE EN POURCENTAGE _________________________//
    /**
     * Méthode qui sert a afficher en pourcentage le taux de foin restant dans les box
     * @return
     */
    public void afficherTauxRemplissage() {
        double tauxRemplissage = calculerTauxRemplissage();
        DecimalFormat df = new DecimalFormat("00.00%");
        String tauxRemplissageFormate = df.format(tauxRemplissage / 100.0);
        System.out.println("Taux de remplissage du box " + boxId + " : " + tauxRemplissageFormate);
    }

    //____________________________________ METHODE POUR AJOUTER UN PONEY A UN BOX ____________________________________//
    /**
     * Méthode qui sert a ajouter un poney a un box
     * @return
     */
    public void ajouterPoney(Poney poney) {
        if (poneyList.size() < capaciter) {
            poneyList.add(poney);
            System.out.println("Le poney " + poney.getNomPoney() + " a été ajouté aux box " + getBoxId() + ".");
        } else {
            System.out.println("La box " + getBoxId() + " est pleine. Impossible d'ajouter le poney " + poney.getNomPoney() + ".");
        }
    }




    //_____ GETTER _____//
    public String getBoxId() {return boxId;}
    public Integer getCapaciter() {return capaciter;}
    public Integer getQuantiterFoinCourant() {return quantiterFoinCourant;}
    public Integer getQuantiterFoinMax() {return quantiterFoinMax;}

    //_____ SETTER _____//
    public void setBoxId(String boxId) {this.boxId = boxId;}
    public void setCapaciter(Integer capaciter) {this.capaciter = capaciter;}
    public void setQuantiterFoinCourant(Integer quantiterFoinCourant) {this.quantiterFoinCourant = quantiterFoinCourant;}
    public void setQuantiterFoinMax(Integer quantiterFoinMax) {this.quantiterFoinMax = quantiterFoinMax;}
}