package ecf.objet;
import java.util.List;
import java.util.ArrayList;

/**
 * Class qui représente le champ
 * @return
 */
public class Champ {
    private List<Poney> poneyList;

    public Champ() {
        this.poneyList = new ArrayList<>();
    }

    /**
     * Méthode qui sert a envoyer les poney paitre
     * @return
     */
    public void ajouterPoney(Poney poney) {
        poneyList.add(poney);
        System.out.println("Le poney " + poney.getNomPoney() + " a été ajouté au champ.");
    }
    /**
     * Méthode qui sert a retiré un poney du champ
     * @return
     */
    public void retirerPoney(Poney poney) {
        poneyList.remove(poney);
    }
    /**
     * Méthode qui vérifie si le poney est dans le champ
     * @return
     */
    public boolean contientPoney(Poney poney) {
        return poneyList.contains(poney);
    }
}
