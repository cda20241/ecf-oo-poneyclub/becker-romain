package ecf.objet;

/**
 * Class qui représente l'entrepot
 * @return
 */
public class Entrepot {
    static Integer stockFoinPrairie;
    static Integer maxFoinPrairie;
    static Integer stockFoinGraminees;
    static Integer maxFoinGraminees;

    public Entrepot(Integer stockFoinPrairie, Integer maxFoinPrairie, Integer stockFoinGraminees, Integer maxFoinGraminees) {
        this.stockFoinPrairie = stockFoinPrairie;
        this.maxFoinPrairie = maxFoinPrairie;
        this.stockFoinGraminees = stockFoinGraminees;
        this.maxFoinGraminees = maxFoinGraminees;
    }

    //_____ GETTER _____//
    public Integer getStockFoinPrairie() {return stockFoinPrairie;}
    public Integer getMaxFoinPrairie() {return maxFoinPrairie;}
    public Integer getStockFoinGraminees() {return stockFoinGraminees;}
    public Integer getMaxFoinGraminees() {return maxFoinGraminees;}

    //_____ SETTER _____//
    public void setStockFoinPrairie(Integer stockFoinPrairie) {this.stockFoinPrairie = stockFoinPrairie;}
    public void setMaxFoinPrairie(Integer maxFoinPrairie) {this.maxFoinPrairie = maxFoinPrairie;}
    public void setStockFoinGraminees(Integer stockFoinGraminees) {this.stockFoinGraminees = stockFoinGraminees;}
    public void setMaxFoinGraminees(Integer maxFoinGraminees) {this.maxFoinGraminees = maxFoinGraminees;}

}
