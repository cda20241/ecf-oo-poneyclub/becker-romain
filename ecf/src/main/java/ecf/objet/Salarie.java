package ecf.objet;

import ecf.enu.Foin;

/**
 * Class salariée qui hérite de la class Personne
 * @return
 */
public class Salarie extends Personne {
    private String poste;
    private String employeur;
    private Entrepot entrepot;

    public Salarie(String nom, String prenom, String poste, String employeur) {
        super(nom, prenom);
        this.poste = poste;
        this.employeur = employeur;
    }


    // Méthode pour réceptionner une livraison

    /**
     * Méthode qui sert a affiché la personne qui a effectuer une reception
     * @param livraison
     * @param personne
     */
    public static void receptionnerLivraison(Livraison livraison, Personne personne) {
        if (Foin.PRAIRIE.equals(livraison.getFoinType())) {
            Entrepot.stockFoinPrairie += livraison.getQuantiterLot();
        } else if (Foin.GRAMINEES.equals(livraison.getFoinType())) {
            Entrepot.stockFoinGraminees += livraison.getQuantiterLot();
        } else {
            System.out.println("Type de foin non reconnu.");
        }
        System.out.println(personne.getNom() + " " + personne.getPrenom() + " a réceptionné une livraison de foin " + livraison.getFoinType());
        System.out.println("Numéro de lot: " + livraison.getNumeroLot());
        System.out.println("Date de livraison: " + livraison.getDateLivraison());
        System.out.println("Quantité: " + livraison.getQuantiterLot() + "Kg");
    }



}
