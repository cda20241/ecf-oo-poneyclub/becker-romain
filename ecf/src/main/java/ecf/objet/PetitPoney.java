package ecf.objet;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ArrayList;

/**
 * Class qui réprésente l'entreprise
 */
public class PetitPoney {
    private static Box box;
    private static Champ champ;
    private final Entrepot entrepot;
    private List<Livraison> livraisonRegistre;
    private final String nomEntreprise;
    private final List<Personne> salarieList;
    private final List<Box> boxList;

    public PetitPoney(String nomEntreprise) {
        this.nomEntreprise = nomEntreprise;
        salarieList = new ArrayList<>();
        boxList = new ArrayList<>();
        livraisonRegistre = Livraison.getLivraisonRegistre();

        // Instance de la class Entrepot
        System.out.println("Création de " + nomEntreprise);
        entrepot = new Entrepot(2000, 5000, 2000, 5000);

        // Instance de la class Champ
        champ = new Champ();

        Box boxA = new Box("A", 3, 200, 600);
        Box boxB = new Box("B", 4, 300, 600);
        Box boxC = new Box("C", 5, 900, 1000);
        Box boxD = new Box("D", 6, 100, 800);
        boxList.add(boxA);
        boxList.add(boxB);
        boxList.add(boxC);
        boxList.add(boxD);

    }


    /**
     * Méthode qui affiche le stock total de foin
     * @return
     */
    public void stockTotal() {
        System.out.println("Foin Prairie: " + entrepot.getStockFoinPrairie() +
                           " | Capacité max: " + entrepot.getMaxFoinPrairie());
        System.out.println("Foin Prairie: " + entrepot.getStockFoinGraminees() +
                           " | Capacité max: " + entrepot.getMaxFoinGraminees());
    }

    /**
     * Méthode qui sert a afficher la liste de tout les boxs
     */
    public void afficherListeDesBox() {
        for (Box box : boxList) {
            System.out.println("Box " + box.getBoxId() +
                    " | Capacité : " + box.getCapaciter() +
                    " | Foin Courant : " + box.getQuantiterFoinCourant() +
                    " | Foin Max : " + box.getQuantiterFoinMax());
        }
    }

    /**
     * Méthode qui permet d'embaucher un ou plusieur salariées
     * @param personne
     */
    public void embaucher(Personne personne){
        salarieList.add(personne);
    }

    /**
     * Méthode qui permet d'afficher tout les salariées de l'entreprise
     * @return
     */
    public void afficherListeEmlpoyer() {
        for (Personne personne : salarieList) {
            System.out.println("Nom " + personne.getNom() + " | Prénom : " + personne.getPrenom());
        }
    }

    /**
     * Méthode qui permet d'attribuer un box a un poney
     * @param box
     * @param poney
     */
    public void attribuerBox(Box box, Poney poney) {
        if (boxList.contains(box)) {
            poney.setBoxAttribue(box);
            System.out.println(poney.getNomPoney() + " a été attribué au box numéro " + box.getBoxId());
        } else {
            System.out.println("La boîte spécifiée n'existe pas dans la liste.");
        }
    }

    /**
     * Méthode qui permet d'afficher le taux de remplisage du foin de tout les box en pourcentage
     * @return
     */
    public void afficherTauxRemplissageDesBoxes() {
        for (Box box : boxList) {
            box.afficherTauxRemplissage();
        }
    }

    /**
     * Méthode qui permet de remplire tout les box a 90% de foin
     * Si un box est deja a 90% on ne le rempli pas
     * @param pourcentage
     */
    public void remplirBox(double pourcentage) {
        for (Box box: boxList) {
            double tauxRemplissage = box.calculerTauxRemplissage();

            if (tauxRemplissage < pourcentage) {
                double quantiterAjouter = (pourcentage / 100) * box.getQuantiterFoinMax() - box.getQuantiterFoinCourant();
                box.setQuantiterFoinCourant(box.getQuantiterFoinCourant() + (int) quantiterAjouter);
                System.out.println("Le box " + box.getBoxId() + " remplie à " + pourcentage + "%");
            } else {
                System.out.println("Le box " + box.getBoxId() + " est deja remplie à " + tauxRemplissage + "%");
            }
        }
    }

    /**
     * Méthode qui permet d'envoyer paitre un ou plusieur poney
     * @param poney
     */
    public void ajouterPoneyAuChamp(Poney poney) {
       champ.ajouterPoney(poney);
    }

    /**
     * Méthode qui permet de rentré les poney dans leur box respectif
     * @param poney
     * @param box
     */
    public void remettrePoneyEnBox(Poney poney, Box box) {
        if (champ.contientPoney(poney)) {
            champ.retirerPoney(poney);
            box.ajouterPoney(poney);
        } else {
            System.out.println("Le poney " + poney.getNomPoney() + " n'est pas dans le champ.");
        }
    }

    /**
     * Méthode qui sert a afficher le registe des livraisons
     * @return
     */
    public static void afficherRegistreLivraisons() {
        Livraison.afficherRegistre();
    }



    //_____ GETTER _____//
    public Box getBox() {return box;}
    public Entrepot getEntrepot() {return entrepot;}
    public String getNomEntreprise() {return nomEntreprise;}
    public List<Personne> getSalarieList() {return salarieList;}
    public List<Box> getBoxList() {return boxList;}

}
