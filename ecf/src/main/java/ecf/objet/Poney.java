package ecf.objet;

import ecf.enu.Criniere;

/**
 * Class qui représente les poney
 * @return
 */
public class Poney {

    private String nomPoney;
    private Integer poidPoney;
    private Integer criniereLongeur;
    private Criniere criniere;
    private Box boxAttribue;

    public Poney(String nomPoney, Integer poidPoney, Integer criniereLongeur, Criniere criniere) {
        this.nomPoney = nomPoney;
        this.poidPoney = poidPoney;
        this.criniereLongeur = criniereLongeur;
        this.criniere = criniere;
        System.out.println(nomPoney + " | Poid: " + poidPoney + "Kg" + ", Crinière " + criniere +
                " de " + criniereLongeur + "cm");
    }

    //_____ GETTER _____//
    public String getNomPoney() {return nomPoney;}
    public Integer getPoidPoney() {return poidPoney;}
    public Integer getCriniereLongeur() {return criniereLongeur;}
    public Criniere getCriniere() {return criniere;}
    public Box getBoxAttribue() {return boxAttribue;}

    //_____ SETTER _____ //
    public void setBoxAttribue(Box box) {this.boxAttribue = box;}
}
