package ecf;
import ecf.enu.Criniere;
import ecf.enu.Foin;
import ecf.objet.*;


public class Main {
    public static void main(String[] args) {

        System.out.println("-----------------------------------------------------------------------------------------");
        // Création de l'entreprise, de l'entrepot et des boxs
        PetitPoney petitPoney = new PetitPoney("Tatooine Poney CLub");
        System.out.println("");

        System.out.println("------------------------------------- LISTE DES BOXS ------------------------------------");
        petitPoney.afficherListeDesBox();
        System.out.println("");

        System.out.println("-------------------------------- AFFICHAGE DU STOCK TOTAL -------------------------------");
        petitPoney.stockTotal();
        System.out.println("");

        System.out.println("------------------------------------- LES SALARIERS -------------------------------------");
        // Instance de la class Personne
        Personne luke = new Personne("SKYWALKER", "Luke");
        Personne obiwan = new Personne("KENOBI", "Obiwan");
        // Embauche des salariées
        petitPoney.embaucher(luke);
        petitPoney.embaucher(obiwan);
        // Affichage des employer de la société
        petitPoney.afficherListeEmlpoyer();
        System.out.println("");

        System.out.println("--------------------------------------- LES PONEYS --------------------------------------");
        // Instance du premier poney
        Poney usain = new Poney("Usain Bolt", 250, 25, Criniere.Lisse);
        // Attribution a un box
        petitPoney.attribuerBox(petitPoney.getBoxList().get(0), usain);
        System.out.println("");
        // Instance du deuxiéme poney
        Poney tonnerre = new Poney("Petit Tonnerre", 280, 28, Criniere.Frisee);
        // Attribution a un box
        petitPoney.attribuerBox(petitPoney.getBoxList().get(1), tonnerre);
        System.out.println("");

        System.out.println("----------------------------------- LIVRAISON DE FOIN -----------------------------------");
        // Instance de la class Livraison
        Livraison livraisonFoin = new Livraison("F003", "14/11/2023", Foin.PRAIRIE, 1500);
        Livraison livraisonFoinG = new Livraison("F004", "04/11/2023", Foin.GRAMINEES, 1500);

        // Récéption par Luke
        Livraison.receptionParSalarie(luke, livraisonFoin);
        System.out.println("");
        Livraison.receptionParSalarie(luke, livraisonFoinG);


        System.out.println("--------------------------------- REGISTRE DES LIVRAISONS --------------------------------");
        PetitPoney.afficherRegistreLivraisons();

        System.out.println("---------------------------------- STOCK APRES LA LIVRAISON ------------------------------");
        petitPoney.stockTotal();
        System.out.println("");

        System.out.println("--------------------- TAUX DE REMPLISSAGE DES BOX AVANT INTERVENTION ---------------------");
        petitPoney.afficherTauxRemplissageDesBoxes();
        System.out.println("");

        System.out.println("--------------------- TAUX DE REMPLISSAGE DES BOX APRES INTERVENTION ---------------------");
        petitPoney.remplirBox(90);
        System.out.println("");

        System.out.println("----------------------------- ON FAIT PRENDRE L'AIR AUX PONEY ----------------------------");
        petitPoney.ajouterPoneyAuChamp(usain);
        petitPoney.ajouterPoneyAuChamp(tonnerre);
        System.out.println("");

        System.out.println("---------------------------- ON RENTRE LES PONEY DANS LES BOXS ----------------------------");
        petitPoney.remettrePoneyEnBox(usain, petitPoney.getBoxList().get(0));
        petitPoney.remettrePoneyEnBox(tonnerre, petitPoney.getBoxList().get(0));
        System.out.println("");

        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("FIN DU PROGRAMME...");
    }

}

